const { emit } = require("process")

var io

function init(io2){
    io = io2
}

function connection(socket) {
    console.log("a user connected")

    socket.on('chat messages', (msg) => {
        //filter out malicious/invalid data
        if((typeof msg) != 'string')
            return
        
        if(msg.length != 5){
            if(msg.length == 'O-O'){
                io.emit('move', 'O-O')
            }else if(msg.length == 'O-O-O'){
                io.emit('move', "O-O-O")
            }
            return;
        }

        //check if the move is valid for this piece
        
    })

    socket.on('disconnect', () => {
        console.log('user disconnected')
    })
}

module.exports = {
    init,
    connection
}