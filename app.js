const express = require('express');
const path = require('path')
const bodyParser = require('body-parser')

const router = require('./router')
const socket = require('./socket')

const app = express();

const middleware = [
    express.static('static'),
    bodyParser.json(),
//    bodyParser.urlencoded({extended: true}),
]

app.use(middleware) 
app.use('/', router)

app.use((req, res, next) => {
    res.send("Error! Page not found. ");
    /*res.status(404).render("error", {
        code: "404",
        reason: "Page Not Found",
        description: "The page you are looking for does not exist."
    })*/
})

const port = 8080

const http = require('http').createServer(app)
const io = require('socket.io')(http)

// Add a connect listener
socket.init(io)
io.on('connection', socket.connection);

http.listen(port, () => {
    console.log(`listening on *:${port}`)
})